#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import jinja2
import os

from google.appengine.ext import db

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja2_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

class Article(db.Model):
    subject = db.StringProperty(required = True)
    content = db.TextProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)

class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_string(self, template, **params):
        t = jinja2_env.get_template(template)
        return t.render(params)
    def render(self, template, **kw):
        self.write(self.render_string(template, **kw))



class MainPage(Handler):
    def render_mainpage(self, subject = "", content = ""):
        articles = db.GqlQuery("SELECT * FROM Article "
                            "ORDER BY created DESC")
        self.render("mainpage.html", subject = subject, content= content, articles = articles)

    def get(self):
        self.render_mainpage()

class NewPost(Handler):
    def render_newpost(self, subject = "", content = "", error = ""):
        self.render("newpost.html", subject = subject, content = content, error = error)


    def get(self):
        self.render_newpost()

    def post(self):
        subject = self.request.get('subject')
        content = self.request.get('content')



        if subject and content:
            a = Article(subject= subject, content = content)
            a.put()
            v = str( a.key().id() )



            self.redirect('/blog/%s' % v)


        else:
            error = "we need bot subject and content."
            self.render_newpost(subject = subject, content = content, error = error)

class Permalink(Handler):
    def get(self, article_id):
        a = Article.get_by_id(int(article_id))
        if a:
            self.render("permalink.html",article = a)







app = webapp2.WSGIApplication([('/blog', MainPage),
                                ('/blog/newpost', NewPost),
                                ('/blog/([0-9]+)', Permalink)],
                                ('/blog/?.json', BlogJsonHandler),debug = True)

