#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import jinja2
import os
import json

from google.appengine.ext import db

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja2_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

import re
import hmac

secret = "hLhANzqevcjJzUAoYbWR"

def render_str(template, **params):
    t = jinja2_environment.get_template(template)
    return t.render(params)


def make_secure_val(val):
    return "%s|%s" % (val, hmac.new(secret, val).hexdigest() )

def check_secure_val(h):
    val = h.split('|')[0]
    if h == make_secure_val(val):
        return val
    else:
        return None

class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_string(self, template, **params):
        t = jinja2_env.get_template(template)
        return t.render(params)
    def render(self, template, **kw):
        self.write(self.render_string(template, **kw))


class User(db.Model):
    username = db.StringProperty()
    password = db.StringProperty()


class LoginHandler(Handler):
    def get(self):
        self.render("login.html", username = "", error_login = "")
    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')
##        self.write(username)
##        self.write(password)
        q = User.gql("WHERE username = :1", username).get()
        error_login = "Invalid login"
        if q:
            if q.password == password:
                user_id = q.key().id()
                new_userid_cookie = make_secure_val( str(user_id) )
                self.response.headers.add_header('Set-Cookie', 'user_id=%s;Path=/' % new_userid_cookie)
##                self.write(q.password)
                self.redirect("/blog/welcome")
            else:
                self.render("login.html",username=username, error_login=error_login)
        else:
            self.render("login.html", username=username, error_login=error_login)

class LogoutHandler(Handler):
    def get(self):
        self.response.headers.add_header('Set-Cookie', 'user_id=;Path=/')
        self.redirect('/blog/signup')


def valid_username(username):

    USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
    return username and USER_RE.match(username)

def valid_password(password):


    USER_RE = re.compile("^.{3,20}$")
    return password and USER_RE.match(password)


def valid_email(user_email):
    if len(user_email) == 0:
        return True
    else:
      USER_RE = re.compile(r"^[\S]+@[\S]+\.[\S]+$")
      return USER_RE.match(user_email)




class SignupHandler(Handler):


    def get(self):
        self.render('signup.html')

    def post(self):
        have_error = False

        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('verify')
        email =  self.request.get('email')

        params = dict( username = username,
                        email = email )



        if not valid_username(username):
            params['error_username'] = "That's not a valid username."
            have_error = True
        elif valid_username(username):
            x = User.gql("WHERE username = :1", username).get()



            if x:

                params['error_user_exists'] = "that user already exists."
                have_error = True
            else:
                have_error = False

        if not valid_password(password):
            params['error_password'] = "That's not a valid password."
            have_error = True
        elif password != verify:
            params['error_verify'] = "Your passwords didn't match."
            have_error = True
        if not valid_email(email):
            params['error_email'] = "That's not a valid email."
            have_error = True

        if have_error:
            self.render('signup.html', **params)
        else:
            user = User(username = username, password = password)
            user.put()
            user_id = user.key().id()
            new_userid_cookie = make_secure_val( str(user_id) )
            self.response.headers.add_header('Set-Cookie', 'user_id=%s;Path=/' % new_userid_cookie)
            self.redirect('/blog/welcome')


class WelcomeHandler(Handler):
    def get(self):

        user_id = check_secure_val( self.request.cookies.get('user_id') )
        if user_id:
            user = User.get_by_id( int (user_id) )

            self.render('welcome.html', username = user.username)
        else:
            self.render('signup.html')


class Article(db.Model):
    subject = db.StringProperty(required = True)
    content = db.TextProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)





class MainPage(Handler):
    def render_mainpage(self, subject = "", content = ""):
        articles = db.GqlQuery("SELECT * FROM Article "
                            "ORDER BY created DESC LIMIT 10")
        self.render("mainpage.html", subject = subject, content= content, articles = articles)

    def get(self):
        self.render_mainpage()

class NewPost(Handler):
    def render_newpost(self, subject = "", content = "", error = ""):
        self.render("newpost.html", subject = subject, content = content, error = error)


    def get(self):
        self.render_newpost()

    def post(self):
        subject = self.request.get('subject')
        content = self.request.get('content')



        if subject and content:
            a = Article(subject= subject, content = content)
            a.put()
            v = str( a.key().id() )



            self.redirect('/blog/%s' % v)


        else:
            error = "we need bot subject and content."
            self.render_newpost(subject = subject, content = content, error = error)

class Permalink(Handler):
    def get(self, article_id):
        a = Article.get_by_id(int(article_id))
        if a:
            self.render("permalink.html",article = a)


class BlogJsonHandler(Handler):
    def get(self):
        j_son = []
        articles = db.GqlQuery("SELECT * FROM Article "
                            "ORDER BY created DESC LIMIT 10")
        articles = list(articles)
        dictionary = {}
        for article in articles:
            dictionary["content"] = article.content
            dictionary["subject"] = article.subject
            j_son.append(dictionary)

            dictionary = {}
##            self.write(article.content)
##            self.write('<br/>')


        self.write(j_son)


class PermalinkJsonHandler(Handler):
    def get(self, article_id):
        article = Article.get_by_id( int(article_id) )
        dictionary = {}
        dictionary["content"] = article.content
        dictionary["subject"] = article.subject
        j_son = []
        j_son.append(dictionary)
        self.write( json.dumps(j_son) )










app = webapp2.WSGIApplication([('/blog', MainPage),
                                ('/blog/newpost', NewPost),
                                ('/blog/([0-9]+)', Permalink),
                                ('/blog/?\.json',BlogJsonHandler),
                                ('/blog/([0-9]+)\.json',PermalinkJsonHandler),
                                ('/blog/signup', SignupHandler),
                                ('/blog/login', LoginHandler),
                                ('/blog/logout', LogoutHandler),
                                ('/blog/welcome', WelcomeHandler)],debug = True)

