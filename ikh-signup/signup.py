#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     10-05-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import webapp2
import jinja2
import os
from string import letters


jinja2_environment = jinja2.Environment(autoescape=True,
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')))

from google.appengine.ext import db


import re

def render_str(template, **params):
    t = jinja2_environment.get_template(template)
    return t.render(params)

class BaseHandler(webapp2.RequestHandler):
    def render(self, template, **kw):
        self.response.out.write(render_str(template, **kw))
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

def valid_username(username):

    USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
    return username and USER_RE.match(username)

def valid_password(password):


    USER_RE = re.compile("^.{3,20}$")
    return password and USER_RE.match(password)


def valid_email(user_email):
    if len(user_email) == 0:
        return True
    else:
      USER_RE = re.compile(r"^[\S]+@[\S]+\.[\S]+$")
      return USER_RE.match(user_email)




class Signup(BaseHandler):


    def get(self):
        self.render('signup.html')

    def post(self):
        have_error = False

        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('verify')
        email =  self.request.get('email')

        params = dict( username = username,
                        email = email )


        if not valid_username(username):
            params['error_username'] = "That's not a valid username."
            have_error = True
        if not valid_password(password):
            params['error_password'] = "That's not a valid password."
            have_error = True
        elif password != verify:
            params['error_verify'] = "Your passwords didn't match."
            have_error = True
        if not valid_email(email):
            params['error_email'] = "That's not a valid email."
            have_error = True

        if have_error:
            self.render('signup.html', **params)
        else:
            self.redirect('/unit2/welcome?username=' + username)








class WelcomeHandler(BaseHandler):
    def get(self):

        username = self.request.get('username')
        if valid_username(username):
            self.render('welcome.html', username = username)
        else:
            self.redirect('/unit2/signup')



app = webapp2.WSGIApplication([('/unit2/signup', Signup), ('/unit2/welcome', WelcomeHandler)],
                              debug=True)
