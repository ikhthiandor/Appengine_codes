#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     10-05-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import webapp2
import jinja2
import os
from string import letters

form = """
    <html>
    <head>
    <title>Users</title>
    </head>
    <body>
    <form method='post'>
    <input type='text' name='user' value='%(user)s'>
    <input type='submit'>
    </form>
    </body>
    </html>
    """


jinja2_environment = jinja2.Environment(autoescape=True,
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')))

from google.appengine.ext import db


import re
import hmac

secret = "hLhANzqevcjJzUAoYbWR"

def render_str(template, **params):
    t = jinja2_environment.get_template(template)
    return t.render(params)


def make_secure_val(val):
    return "%s|%s" % (val, hmac.new(secret, val).hexdigest() )

def check_secure_val(h):
    val = h.split('|')[0]
    if h == make_secure_val(val):
        return val
    else:
        return None


class User(db.Model):
    username = db.StringProperty()
    password = db.StringProperty()



class BaseHandler(webapp2.RequestHandler):
    def render(self, template, **kw):
        self.response.out.write(render_str(template, **kw))
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

class UsersHandler(BaseHandler):
##    def write_form(user=''):
##        self.write(form % {"user": user})

    def get(self):

        users = User.all()
        for user in users:
            self.write('<br>')
            self.write(str(user.username) + " " + str(user.password) )

class LoginHandler(BaseHandler):
    def get(self):
        self.render("login.html", username = "", error_login = "")
    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')
##        self.write(username)
##        self.write(password)
        q = User.gql("WHERE username = :1", username).get()
        error_login = "Invalid login"
        if q:
            if q.password == password:
                user_id = q.key().id()
                new_userid_cookie = make_secure_val( str(user_id) )
                self.response.headers.add_header('Set-Cookie', 'user_id=%s;Path=/' % new_userid_cookie)
##                self.write(q.password)
                self.redirect("/welcome")
            else:
                self.render("login.html",username=username, error_login=error_login)
        else:
            self.render("login.html", username=username, error_login=error_login)

class LogoutHandler(BaseHandler):
    def get(self):
        self.response.headers.add_header('Set-Cookie', 'user_id=;Path=/')
        self.redirect('/signup')

##        user = ''
##
##        self.write(form % {'user' : user})
##    def post(self):
##        users = db.GqlQuery('select * from User')
##
##        user = self.request.get('user')
##        if user in users:
##            self.write('User Exists')
##        else:
##            self.write("Nope")



def valid_username(username):

    USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
    return username and USER_RE.match(username)

def valid_password(password):


    USER_RE = re.compile("^.{3,20}$")
    return password and USER_RE.match(password)


def valid_email(user_email):
    if len(user_email) == 0:
        return True
    else:
      USER_RE = re.compile(r"^[\S]+@[\S]+\.[\S]+$")
      return USER_RE.match(user_email)




class Signup(BaseHandler):


    def get(self):
        self.render('signup.html')

    def post(self):
        have_error = False

        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('verify')
        email =  self.request.get('email')

        params = dict( username = username,
                        email = email )


##        self.write(users)


        if not valid_username(username):
            params['error_username'] = "That's not a valid username."
            have_error = True
        elif valid_username(username):
            x = User.gql("WHERE username = :1", username).get()


##            user = User.gql("WHERE username = :1", username)

            if x:

                params['error_user_exists'] = "that user already exists."
                have_error = True
            else:
                have_error = False

        if not valid_password(password):
            params['error_password'] = "That's not a valid password."
            have_error = True
        elif password != verify:
            params['error_verify'] = "Your passwords didn't match."
            have_error = True
        if not valid_email(email):
            params['error_email'] = "That's not a valid email."
            have_error = True

        if have_error:
            self.render('signup.html', **params)
        else:
            user = User(username = username, password = password)
            user.put()
            user_id = user.key().id()
            new_userid_cookie = make_secure_val( str(user_id) )
            self.response.headers.add_header('Set-Cookie', 'user_id=%s;Path=/' % new_userid_cookie)
            self.redirect('/welcome')








class WelcomeHandler(BaseHandler):
    def get(self):

        user_id = check_secure_val( self.request.cookies.get('user_id') )
        if user_id:
            user = User.get_by_id( int (user_id) )

            self.render('welcome.html', username = user.username)
        else:
            self.render('signup.html')








app = webapp2.WSGIApplication([('/signup', Signup), ('/welcome', WelcomeHandler), ('/users', UsersHandler), ('/login', LoginHandler), ('/logout', LogoutHandler)],
                              debug=True)
