#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2

form = """
<html>
  <head>
    <title>Rot 13</title>
  </head>

  <body>
    <h2>Enter some text to ROT13:</h2>
    <form method="post">
      <textarea name="text"
                style="height: 100px; width: 400px;">%s</textarea>
      <br>
      <input type="submit">
    </form>
  </body>

</html>

"""



def rot13_out(user_text):
    output = ''
    for ch in user_text:

        if (ch.isalpha()):
            output += rot13(ch)
        else:
            output += ch

    return output


def rot13(ch):
    if ch.islower():
        if (ord(ch) + 13 - ord('a')) < 26:
            return chr(ord(ch) + 13)
        else:
            return chr(ord(ch) - 13)
    else:
        if (ord(ch) + 13 - ord('A')) < 26:
            return chr(ord(ch) + 13)
        else:
            return chr(ord(ch) - 13)

def escape_html(text):
    for (i, o) in ( ("&", "&amp;"),
                    (">", "&gt;"),
                    ("<","&lt;"),
                    ("\"","&quot;")):
                        text = text.replace(i,o)
    return text


class MainHandler(webapp2.RequestHandler):
    def write_form(self,text=""):
        self.response.write(form % (text))
    def get(self):
        self.write_form()
    def post(self):
        user_text = self.request.get('text')
        # self.response.out.write("User response was %s" % (user_text))
        rot13_text = rot13_out(user_text)
        escaped_html_text = escape_html(rot13_text)
        self.write_form(escaped_html_text)



app = webapp2.WSGIApplication([('/', MainHandler)],
                              debug=True)
